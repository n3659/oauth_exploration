pub trait Identifiable {
	 fn its_him(&self, id: usize) -> bool;
	fn id(&self) -> usize;
}

pub trait Actionnable {
	fn execute(&mut self);
}

pub trait EventLoopSubscriber: Identifiable + Actionnable {}
