use std::cell::RefCell;
use std::collections::LinkedList;
use nix::sys::event::{EventFlag, FilterFlag, kevent, KEvent, kqueue};
use std::os::fd::RawFd;
use std::rc::Rc;
use nix::sys::event::EventFilter::EVFILT_READ;
use crate::event_loop::event_loop_subscriber::EventLoopSubscriber;

pub struct EventLoop {
	kq_fd: RawFd,
	subscribers:LinkedList<Rc<RefCell<dyn EventLoopSubscriber>>>,
}

impl EventLoop {
	pub fn new() -> Self {
		Self {
			kq_fd: kqueue().expect("Echec de l'initialisation de la boucle événementiel"),
			subscribers: LinkedList::new()
		}
	}

	pub fn subscribe(&mut self, subscriber: Rc<RefCell<dyn EventLoopSubscriber>>) {
		let event = init_event(subscriber.borrow_mut().id());

		self.subscribers.push_back(subscriber);

		kevent(self.kq_fd, &[event], &mut [], 0).expect("Echec observation des nouvelles connexions");
	}

	pub fn launch(&self) {
		let mut event_list = [init_event(0); 1];

		loop {
			kevent(self.kq_fd, &[], &mut event_list, 0).expect("Echec observation des nouvelles connexions");

			event_list.iter().for_each(|event| self.handle(event));
		}
	}

	fn handle(&self, event:&KEvent) {
		let id = event.ident();
		let subscriber = self.subscribers.iter().find(|item|item.borrow_mut().its_him(id));

		if subscriber.is_some() {
			subscriber.unwrap().borrow_mut().execute();
		}
	}
}

fn init_event(fd: usize) -> KEvent {
	KEvent::new(fd, EVFILT_READ, EventFlag::EV_ADD | EventFlag::EV_CLEAR | EventFlag::EV_ENABLE, FilterFlag::empty(), 0, 0)
}
