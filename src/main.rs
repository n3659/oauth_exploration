mod event_loop;
mod server;

fn main() {
	let mut server = server::server::Server::new("127.0.0.1:8080");

	server.launch();
}
