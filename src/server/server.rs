use std::cell::RefCell;
use std::net::{ToSocketAddrs};
use std::rc::Rc;
use crate::event_loop::event_loop::EventLoop;
use super::event_loop_subscriber::server_subscriber::ServerSubscriber;

pub struct Server {
	event_loop: Rc<RefCell<EventLoop>>,
}

impl Server {
	pub fn new<A: ToSocketAddrs>(addr: A) -> Self {
		let event_loop = Rc::new(RefCell::new(EventLoop::new()));

		event_loop.borrow_mut().subscribe(Rc::new(RefCell::new(ServerSubscriber::new(addr, event_loop.clone()))));

		Self {
			event_loop: event_loop.clone(),
		}
	}

	pub fn launch(&mut self) {
		println!("serveur lancé");
		self.event_loop.borrow_mut().launch();
	}
}
