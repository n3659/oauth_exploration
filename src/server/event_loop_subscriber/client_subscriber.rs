use std::io::Read;
use std::net::TcpStream;
use std::os::fd::AsRawFd;
use crate::event_loop::event_loop_subscriber::{Actionnable, EventLoopSubscriber, Identifiable};

pub struct ClientSubscriber {
	client:TcpStream
}

impl ClientSubscriber {
	pub fn new(client:TcpStream) -> Self {
		Self {
			client
		}
	}
}

impl Identifiable for ClientSubscriber {
	fn its_him(&self, id: usize) -> bool {
		 self.id() == id
	}

	fn id(&self) -> usize {
		self.client.as_raw_fd() as usize
	}
}

impl Actionnable for ClientSubscriber {
	fn execute(&mut self) {
		let buffer = &mut [0;1024];
		self.client.read(buffer).expect("echec: client");
		println!("client: {buffer:?}")
	}
}

impl EventLoopSubscriber for ClientSubscriber {}
