use std::cell::RefCell;
use std::net::{TcpListener, ToSocketAddrs};
use std::os::fd::AsRawFd;
use std::rc::Rc;
use crate::event_loop::event_loop::EventLoop;
use crate::event_loop::event_loop_subscriber::{Actionnable, EventLoopSubscriber, Identifiable};
use crate::server::event_loop_subscriber::client_subscriber::ClientSubscriber;

pub struct ServerSubscriber {
	listener: TcpListener,
	pub event_loop: Rc<RefCell<EventLoop>>,
}

impl ServerSubscriber {
	pub fn new<A: ToSocketAddrs>(addr: A, event_loop: Rc<RefCell<EventLoop>>) -> Self {
		Self {
			listener: TcpListener::bind(addr).expect("Echec de l'initialisation du serveur"),
			event_loop,
		}
	}
}

impl Identifiable for ServerSubscriber {
	fn its_him(&self, id: usize) -> bool {
		self.listener.as_raw_fd() as usize == id
	}

	fn id(&self) -> usize {
		self.listener.as_raw_fd() as usize
	}
}

impl Actionnable for ServerSubscriber {
	fn execute(&mut self) {
		match self.listener.accept() {
			Ok((client_stream, _addr)) => self.event_loop.borrow_mut().subscribe(Rc::new(RefCell::new(ClientSubscriber::new(client_stream)))),
			Err(e) => println!("couldn't get client: {e:?}"),
		}
	}
}

impl EventLoopSubscriber for ServerSubscriber {

}
